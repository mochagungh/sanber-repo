// Soal 1
const hitungLuasPersegiPanjang = (panjang, lebar) => {
  const p = panjang;
  const l = lebar;
  return p * l;
};
const hitungKelilingPersegiPanjang = (panjang, lebar) => {
  const p = panjang;
  const l = lebar;
  return 2 * (p + l);
};
console.log(hitungLuasPersegiPanjang(3, 7));
console.log(hitungKelilingPersegiPanjang(3, 7));

// atau
const luasDanKelilingPersegiPanjang = (panjang, lebar) => {
  const p = panjang;
  const l = lebar;
  const luas = p * l;
  const keliling = 2 * (p + l);
  return { luas, keliling };
};
console.log(luasDanKelilingPersegiPanjang(3, 7));

// Soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(firstName + " " + lastName);
    },
  };
};

//Driver Code
newFunction("William", "Imoh").fullName();

// Soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};
const { firstName, lastName, address, hobby } = newObject;
// Driver code
console.log(firstName, lastName, address, hobby);

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

// Soal 5
const planet = "earth";
const view = "glass";
var before =
  "Lorem " +
  view +
  "dolor sit amet, " +
  "consectetur adipiscing elit," +
  planet;
const after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`;

console.log(before);
console.log(after);
