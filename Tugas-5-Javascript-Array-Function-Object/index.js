// Soal 2
function introduce(params) {
  var str;
  str =
    "Nama saya " +
    params.name +
    ", umur saya " +
    params.age +
    " tahun, alamat saya di " +
    params.address +
    ", dan saya punya hobby yaitu " +
    params.hobby;
  return str;
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan);

// Soal 3
function hitung_huruf_vokal(str) {
  return (str.match(/[aeiou]/gi) || []).length;
}
var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2); // 3 2
