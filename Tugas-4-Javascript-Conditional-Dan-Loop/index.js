// Soal 1
var nilai = 55;

if (nilai >= 85) {
  console.log("A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("D");
} else {
  console.log("E");
}

// Soal 2
var tanggal = 8;
var bulan = 9;
var tahun = 1996;

var strTanggal = tanggal.toString();
var strTahun = tahun.toString();
switch (bulan) {
  case 1: {
    console.log(strTanggal + " Januari " + strTahun);
    break;
  }
  case 2: {
    console.log(strTanggal + " Februari " + strTahun);
    break;
  }
  case 3: {
    console.log(strTanggal + " Maret " + strTahun);
    break;
  }
  case 4: {
    console.log(strTanggal + " April " + strTahun);
    break;
  }
  case 5: {
    console.log(strTanggal + " Mei " + strTahun);
    break;
  }
  case 6: {
    console.log(strTanggal + " Juni " + strTahun);
    break;
  }
  case 7: {
    console.log(strTanggal + " Juli " + strTahun);
    break;
  }
  case 8: {
    console.log(strTanggal + " Agustus " + strTahun);
    break;
  }
  case 9: {
    console.log(strTanggal + " September " + strTahun);
    break;
  }
  case 10: {
    console.log(strTanggal + " Oktober " + strTahun);
    break;
  }
  case 11: {
    console.log(strTanggal + " November " + strTahun);
    break;
  }
  case 12: {
    console.log(strTanggal + " Desember " + strTahun);
    break;
  }

  default:
    console.log(strTanggal + " Bulan " + strTahun);
    break;
}

// Soal 3
// var n = 3;
// for (i = 1; i <= n; i++) {
//   for (var j = 1; j <= i; j++) {
//     console.log("#");
//   }
// }
